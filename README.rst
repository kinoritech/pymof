pymof
=====

pymof is an implementation of the `MOF 2.4.1 <http://www.omg.org/mof/>`__ standard. Form their description:

    The MetaObject Facility Specification is the foundation of OMG's
    industry-standard environment where models can be exported from one
    application, imported into another, transported across a network,
    stored in a repository and then retrieved, rendered into different
    formats (including XMI, OMG's XML-based standard format for model
    transmission and storage), transformed, and used to generate
    application code. These functions are not restricted to structural
    models, or even to models defined in UML - behavioral models and
    data models also participate in this environment, and non-UML
    modelling languages can partake also, as long as they are MOF-based.

The intention of this project is to encourage the development of
`modelling <http://www.omg.org/mda/>`__ tools (such as Model
Transformation Languages) with Python.
